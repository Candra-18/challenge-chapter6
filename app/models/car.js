'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  car.init({
    type: DataTypes.STRING,
    model: DataTypes.STRING,
    rentperday: DataTypes.INTEGER,
    image: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updateBy: DataTypes.STRING,
    available: DataTypes.BOOLEAN,
    availableAt: DataTypes.STRING,
    deleteBy:DataTypes.STRING,
    isDeleted:DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'car',
  });
  return car;
};