const jwt = require('jsonwebtoken');
const usersService = require("../services/user")

module.exports = {
    async authorize(req, res, next) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(
                token,
                process.env.JWT_PRIVATE_KEY || "bukanapaapa"
            );
            req.user = await usersService.get(tokenPayload.id);
            next();
        } catch (err) {
            res.status(401).json({
                error: err.message,
                message: "you have to login first before doing it!"
            });
        }
    },

    
    async checkSuperAdmin(req, res, next) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(
                token,
                process.env.JWT_PRIVATE_KEY || "bukanapaapa"
            );

            req.user = await usersService.get(tokenPayload.id);
            if (!req.user.isSuperAdmin) {
                res.status(401).json({
                    status: 'FAIL',
                    message: 'you are not superadmin!!'
                })
                return;
            }
            next();
        } catch (err) {
            res.status(401).json({
                error: err.message,
                message: "you have to login first before doing it!"
            });
        }
    },

    async checkAdmin(req, res, next) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(
                token,
                process.env.JWT_PRIVATE_KEY || "bukanapaapa"
            );

            req.user = await usersService.get(tokenPayload.id);
            if (!req.user.isAdmin) {
                res.status(401).json({
                    status: 'FAIL',
                    message: 'You are not Admin!'
                })

                return;
            }
            next();
        } catch (err) {
            res.status(401).json({
                error: err.message,
                message: "you have to login first before doing it!"
            });
        }
    },
}
