const check = require("./check");
const checkKeberadaan = require("./checkKeberadaan");
const authorization = require("./authorization");


module.exports = {
    check,
    checkKeberadaan,
    authorization
};