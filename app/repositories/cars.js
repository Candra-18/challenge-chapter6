const {car} = require ("../models")

module.exports = {
    create(inputData) {
        return car.create(inputData);
    },
    findOne(key) {
        return car.findOne(key);
    },
    findAll(args) {
        return car.findAll(args);
    },
    
    getTotalcars() {
        return car.count();
    },
    find(id) {
        return car.findByPk(id);
      },
      update(id, updateArgs) {
        return car.update(updateArgs, {
          where: {
            id,
          },
        });
      },
      delete(id, updateArgs) {
        return car.update(updateArgs, {
          where: {
            id,
          },
        });
      },


}