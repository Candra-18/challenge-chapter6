const { user } = require("../models");

module.exports = {
    create(inputData) {
        return user.create(inputData);
    },
    findOne(key) {
        return user.findOne(key);
    },
    find(id) {
        return user.findByPk(id);
    },
    update(id, updatedData) {
        return user.update(updatedData, {
            where: {
                id,
            },
        });
    },


};