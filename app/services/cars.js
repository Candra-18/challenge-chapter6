const carsRepository = require ("../repositories/cars");

module.exports = {
    create(requestBody) {
        return carsRepository.create(requestBody);
    },
    getOne(key) {
        return carsRepository.findOne(key);
    },
    async list(args) {
        try {
          const posts = await carsRepository.findAll(args);
          const postCount = await carsRepository.getTotalcars();
    
          return {
            data: posts,
            count: postCount,
          };
        } catch (err) {
          throw err;
        }
      },

      get(id) {
        return carsRepository.find(id);
      },
      update(id, requestBody) {
        return carsRepository.update(id, requestBody);
      },
      delete(id, requestBody) {
        return carsRepository.delete(id, requestBody);
      },
}