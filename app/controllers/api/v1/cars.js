const carsService = require("../../../services/cars")

module.exports = {
    async createCars(req, res) {
        req.body.createdBy = req.user.email;
        carsService.create(req.body).then((createdCar) => {
            res.status(201).json({
                status: "Success",
                message: 'Car Success Created by ${req.user.email}',
                data: createdCar,
            });
        }).catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            });
        });
    },
    list(req, res) {
      carsService
          .list({
            where:{isDeleted:false}
          })
          .then(({ data, count }) => {
            res.status(200).json({
              status: "OK",
              data: { posts: data },
              meta: { total: count },
            });
          })
          .catch((err) => {
            res.status(400).json({
              status: "FAIL",
              message: err.message,
            });
          });
      },
      showcar(req, res) {
        carsService
          .get(req.params.id)
          .then((post) => {
            res.status(200).json({
              status: "OK",
              data: post,
            });
          })
          .catch((err) => {
            res.status(422).json({
              status: "FAIL",
              message: err.message,
            });
          });
      },
      update(req, res) {
        carsService
          .update(req.params.id, req.body)
          .then(() => {
            res.status(200).json({
              status: "OK",
            });
          })
          .catch((err) => {
            res.status(422).json({
              status: "FAIL",
              message: err.message,
            });
          });
      },
      destroy(req, res) {
        carsService
          .delete(req.params.id, { isDeleted:true, deleteBy: req.user.email })
          .then(() => {
            res.status(200).json({
              deleteBy: req.user.email
            });
            
          })
          .catch((err) => {
            res.status(422).json({
              status: "FAIL",
              message: err.message,
            });
          });
      },
    

};
