/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */


const user = require("./user");
const cars = require("./cars");

module.exports = {
  user,
  cars
 
};
