const express = require("express");
const controllers = require("../app/controllers");
const middlewares = require("../app/middlewares");
const appRoute = express.Router();


const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../data/swagger.json");
// const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */
// apiRouter.get("/api/v1/posts", controllers.api.v1.postController.list);
// apiRouter.post("/api/v1/posts", controllers.api.v1.postController.create);
// apiRouter.put("/api/v1/posts/:id", controllers.api.v1.postController.update);
// apiRouter.get("/api/v1/posts/:id", controllers.api.v1.postController.show);
// apiRouter.delete(
//   "/api/v1/posts/:id",
//   controllers.api.v1.postController.destroy
// );

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
// apiRouter.get("/api/v1/errors", () => {
//   throw new Error(
//     "The Industrial Revolution and its consequences have been a disaster for the human race."
//   );
// });

// apiRouter.use(controllers.api.main.onLost);
// apiRouter.use(controllers.api.main.onError);


// API routes user 
appRoute.post("/api/v1/register",
middlewares.check.checkCondition,
controllers.api.v1.user.register
);

appRoute.post("/api/v1/login",
middlewares.checkKeberadaan.checkData,
controllers.api.v1.user.login
);

appRoute.get("/api/v1/dashboard",
    middlewares.authorization.authorize,
    controllers.api.v1.user.whoAmI
);

appRoute.put("/api/v1/users/:id/update-admin",
    middlewares.authorization.checkSuperAdmin,
    controllers.api.v1.user.intoAdmin
);

// API routes cars

appRoute.post("/api/v1/cars",
    middlewares.authorization.checkAdmin,
    controllers.api.v1.cars.createCars
);
appRoute.get("/api/v1/cars",
    middlewares.authorization.authorize,
    controllers.api.v1.cars.list
);

appRoute.get("/api/v1/cars/:id",
    middlewares.authorization.authorize,
    controllers.api.v1.cars.showcar
);

appRoute.put("/api/v1/cars/:id",
    middlewares.authorization.checkAdmin,
    controllers.api.v1.cars.update
);
appRoute.delete("/api/v1/cars/:id",
    middlewares.authorization.checkAdmin,
    controllers.api.v1.cars.destroy
);



// swager 

appRoute.use("/api-docs", swaggerUi.serve);
appRoute.get("/api-docs", swaggerUi.setup(swaggerDocument));





module.exports = appRoute;